import ComponentA from "./ComponentA"

// 子组件向父组件传值
const Test2 = () => {
    return (
        <ComponentA></ComponentA>
    )
}

export default Test2
