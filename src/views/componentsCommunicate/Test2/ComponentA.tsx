import { Button, Row } from "antd"
import { useRef, useState } from "react"
import ComponentB from "./ComponentB"

const ComponentA = () => {
    const [value1, setValue1] = useState<string>("")
    const componentBRef = useRef<any>()

    const onValue1Change = (value: string) => {
        setValue1(value)
    }

    const getMessage = () => {
        console.log("输入框1：", value1)
        console.log("输入框2：", componentBRef.current.value2)
    }

    return (
        <Row
            style={{
                width: 600,
                height: 500,
                padding: 20,
                flexDirection: "column",
                background: "#f3829630",
                justifyContent: 'flex-start',
            }}
            className="v-flex-center h-padding-8"
        >
            <Row justify="center"><h1>ComponentA</h1></Row>
            <Button style={{ width: 200 }} onClick={getMessage}>获取B组件输入内容</Button>
            <Row justify="space-around" style={{ marginTop: 32 }}>
                <ComponentB ref={componentBRef} value1={value1} onValue1Change={onValue1Change}></ComponentB>
            </Row>
        </Row >
    )
}

export default ComponentA
