import { Input, Row } from "antd"
import { forwardRef, useImperativeHandle, useState } from "react"

interface IComponentB {
    value1: string,
    onValue1Change: (value: string) => void
}

const ComponentB = forwardRef((props: IComponentB, ref) => {
    const { value1, onValue1Change } = props
    const [value2, setValue2] = useState<string>("")

    useImperativeHandle(ref, () => ({
        value2
    }));

    return (
        <Row className="v-flex-center h-padding-8" style={{ width: 300, height: 300, background: "#00f1ff4d" }} gutter={[16, 16]}>
            <h3 className="w100 text_center">
                ComponentB
            </h3>
            <Row className="w100" align="middle" wrap={false}>
                <label>输入框1：</label>
                <Input value={value1} onChange={(e) => onValue1Change(e.target.value)}></Input>
            </Row>
            <Row className="w100" align="middle" wrap={false}>
                <label>输入框2：</label>
                <Input className="w100" value={value2} onChange={(e) => setValue2(e.target.value)}></Input>
            </Row>
        </Row>
    )
})

export default ComponentB
