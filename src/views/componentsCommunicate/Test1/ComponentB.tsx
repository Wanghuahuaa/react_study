import { Row } from "antd"

const ComponentB = (props: any) => {
    const { pageInfo } = props

    return (
        <Row justify="center" align="middle" style={{ width: 220, height: 220, background: "#00f1ff4d" }}>
            <h3 className="w100" style={{ textAlign: "center" }}>
                ComponentB
                <br />
                <h5>菜单名称：{pageInfo.menuTitle}</h5>
            </h3>
        </Row>
    )
}

export default ComponentB
