import { Row } from "antd"
import ComponentB from "./ComponentB"

const ComponentA = () => {
    const pageInfo = {
        menuTitle: "父组件向子组件传值"
    }

    return (
        <Row
            style={{
                width: 600,
                height: 500,
                padding: 20,
                flexDirection: "column",
                background: "#f3829630"
            }}
            justify="start"
        >
            <Row justify="center"><h1>ComponentA</h1></Row>
            <Row justify="space-around" style={{ marginTop: 32 }}>
                <ComponentB pageInfo={pageInfo}></ComponentB>
            </Row>
        </Row >
    )
}

export default ComponentA
