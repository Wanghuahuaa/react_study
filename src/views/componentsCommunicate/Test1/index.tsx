import ComponentA from "./ComponentA"

// 父组件向子组件传值
const Test1 = () => {
    return (
        <ComponentA></ComponentA>
    )
}

export default Test1
