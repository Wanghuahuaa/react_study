import { Row } from "antd"

const ComponentB = (props: any) => {
    const { count } = props

    return (
        <Row className="v-flex-center" justify="center" align="middle" style={{ width: 220, height: 220, background: "#00f1ff4d" }}>
            <h3>ComponentB</h3>
            count: {count}
        </Row>
    )
}

export default ComponentB
