import { Button } from "antd"

const ComponentC = (props: any) => {
    const { setCount } = props

    const add = () => {
        setCount((prev: number) => prev + 1)
    }

    return (
        <div className="v-flex-center" style={{ width: 220, height: 220, background: "#00f1ff4d" }}>
            <h3>ComponentC</h3>
            <Button onClick={add}>count+1</Button>
        </div>
    )
}

export default ComponentC
