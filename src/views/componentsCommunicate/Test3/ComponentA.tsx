import { Row } from "antd"
import { useState } from "react"
import ComponentB from "./ComponentB"
import ComponentC from "./ComponentC"

const ComponentA = () => {
    const [count, setCount] = useState<number>(0)

    return (
        <Row
            style={{
                width: 600,
                height: 500,
                padding: 20,
                flexDirection: "column",
                background: "#f3829630"
            }}
            justify="start"
        >
            <Row justify="center"><h1>ComponentA</h1></Row>
            <Row justify="space-around" style={{ marginTop: 32 }}>
                <ComponentB count={count}></ComponentB>
                <ComponentC setCount={setCount}></ComponentC>
            </Row>
        </Row >
    )
}

export default ComponentA
