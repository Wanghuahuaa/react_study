import ComponentD from "./ComponentD"

const ComponentC = (props: any) => {
    const { setCount } = props

    return (
        <div className="v-flex-center" style={{ width: 250, height: 250, background: "#00f1ff4d" }}>
            <h3>ComponentC</h3>
            <ComponentD></ComponentD>
        </div>
    )
}

export default ComponentC
