import { Row } from "antd";
import PubSub from "pubsub-js";
import { useEffect, useState } from "react";

const ComponentB = () => {
    const [inputValue, setInputValue] = useState<string>("")

    useEffect(() => {
        const sId = PubSub.subscribe("GET_INPUT_VALUE", (_, data: any) => {
            setInputValue(data.value);
        });
        return () => {
            PubSub.unsubscribe(sId);
        }
    }, [])

    return (
        <Row className="v-flex-center" justify="center" align="middle" style={{ width: 250, height: 250, background: "#00f1ff4d" }}>
            <h3>ComponentB</h3>
            <div>D组件输入内容: <b>{inputValue || "--"}</b></div>
        </Row>
    )
}

export default ComponentB
