import { Button, Input } from "antd"
import { useState } from "react"

const ComponentD = () => {
    const [value, setValue] = useState<string>("")

    const updateValue = () => {
        PubSub.publish("GET_INPUT_VALUE", { value });
    }

    return (
        <div className="v-flex-center h-padding-8" style={{ width: 180, height: 180, background: "#ffffff" }}>
            <h5>ComponentD</h5>
            <div>
                <Input value={value} onChange={(e) => setValue(e.target.value)}></Input>
            </div>
            <Button style={{ marginTop: 16 }} onClick={updateValue}>更新B组件显示内容</Button>
        </div>
    )
}

export default ComponentD
