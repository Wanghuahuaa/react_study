import { Typography } from "antd";

const ErrorPage = () => {
    return (
        <div>
            <Typography.Title >
                404
            </Typography.Title>
        </div>
    )
}

export default ErrorPage
