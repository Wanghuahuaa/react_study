import { Route, Routes } from "react-router-dom";
import Detail from "./Detail";
import List from "./List";

const ReactRouter = () => {
    return (
        <div>
            <Routes>
                <Route path="/" element={<List />}></Route>
                <Route path="/detail/:id" element={<Detail />}></Route>
            </Routes>
        </div>
    )
}

export default ReactRouter
