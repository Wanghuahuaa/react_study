import { Col, Row, Typography } from "antd";
import { useNavigate } from "react-router-dom";
import "./index.less";

const List = () => {
    const navigate = useNavigate();

    const useList = [
        {
            id: 0,
            name: "王花花",
            tel: 15555555555,
            introduction: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique provident recusandae nemo sint. Accusamus saepe atque, necessitatibus repellendus rerum at laudantium ullam consectetur voluptatum voluptas. Qui aliquam consectetur accusamus recusandae!  Assumenda, velit nostrum facilis alias nobis, mollitia accusantium id placeat deleniti maxime fugit doloremque, beatae repellat obcaecati voluptates quisquam vel nihil. Reiciendis libero praesentium error. Vero dolore cum odio nesciunt!  Voluptatem id labore quos molestiae rerum, numquam voluptate quam voluptates assumenda deleniti laboriosam rem vitae possimus deserunt ipsam corrupti nulla et. Mollitia quos tenetur dignissimos praesentium est dolorem perspiciatis sapiente!
            Repellat voluptatum quasi, odio dolore ipsum minima, fuga distinctio accusamus dolores nulla quae est numquam, in consectetur molestiae doloremque iusto aspernatur. Enim officia quidem culpa tempora nesciunt corporis error corrupti. Hic doloribus voluptates vitae repellendus, suscipit architecto consequatur laboriosam odit nesciunt dolor corporis iusto accusantium itaque libero veritatis quaerat molestiae dicta non. A quibusdam vero dolor sint voluptatem doloribus facere?`,
        },
        {
            id: 1,
            name: "李栓蛋",
            tel: 18888888888,
            introduction: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique provident recusandae nemo sint. Accusamus saepe atque, necessitatibus repellendus rerum at laudantium ullam consectetur voluptatum voluptas. Qui aliquam consectetur accusamus recusandae!  Assumenda, velit nostrum facilis alias nobis, mollitia accusantium id placeat deleniti maxime fugit doloremque, beatae repellat obcaecati voluptates quisquam vel nihil. Reiciendis libero praesentium error. Vero dolore cum odio nesciunt!  Voluptatem id labore quos molestiae rerum, numquam voluptate quam voluptates assumenda deleniti laboriosam rem vitae possimus deserunt ipsam corrupti nulla et. Mollitia quos tenetur dignissimos praesentium est dolorem perspiciatis sapiente!
            Repellat voluptatum quasi, odio dolore ipsum minima, fuga distinctio accusamus dolores nulla quae est numquam, in consectetur molestiae doloremque iusto aspernatur. Enim officia quidem culpa tempora nesciunt corporis error corrupti. Hic doloribus voluptates vitae repellendus, suscipit architecto consequatur laboriosam odit nesciunt dolor corporis iusto accusantium itaque libero veritatis quaerat molestiae dicta non. A quibusdam vero dolor sint voluptatem doloribus facere?`
        },
        {
            id: 2,
            name: "张富贵",
            tel: 19999999999,
            introduction: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique provident recusandae nemo sint. Accusamus saepe atque, necessitatibus repellendus rerum at laudantium ullam consectetur voluptatum voluptas. Qui aliquam consectetur accusamus recusandae!  Assumenda, velit nostrum facilis alias nobis, mollitia accusantium id placeat deleniti maxime fugit doloremque, beatae repellat obcaecati voluptates quisquam vel nihil. Reiciendis libero praesentium error. Vero dolore cum odio nesciunt!  Voluptatem id labore quos molestiae rerum, numquam voluptate quam voluptates assumenda deleniti laboriosam rem vitae possimus deserunt ipsam corrupti nulla et. Mollitia quos tenetur dignissimos praesentium est dolorem perspiciatis sapiente!
            Repellat voluptatum quasi, odio dolore ipsum minima, fuga distinctio accusamus dolores nulla quae est numquam, in consectetur molestiae doloremque iusto aspernatur. Enim officia quidem culpa tempora nesciunt corporis error corrupti. Hic doloribus voluptates vitae repellendus, suscipit architecto consequatur laboriosam odit nesciunt dolor corporis iusto accusantium itaque libero veritatis quaerat molestiae dicta non. A quibusdam vero dolor sint voluptatem doloribus facere?`
        },
    ]

    const toDetail = (user: any) => {
        navigate(`/reactRouter/detail/${user.id}?name=${user.name}&tel=${user.tel}`, { state: { user } });
    }

    return (
        <div className="react-router-page">
            <Typography.Title level={3}>用户列表</Typography.Title>
            {
                useList.map((user) => (<Row
                    key={user.id}
                    className="pointer row-hover v-padding-4 h-padding-8"
                    style={{ width: 350 }}
                    wrap={false}
                    onClick={() => toDetail(user)}
                >
                    <Col style={{ marginRight: 32 }}>
                        <Typography.Paragraph>
                            <Typography.Text strong>id：</Typography.Text>
                            {user.id}
                        </Typography.Paragraph>
                    </Col>
                    <Col style={{ marginRight: 32 }}>
                        <Typography.Paragraph>
                            <Typography.Text strong>姓名：</Typography.Text>
                            {user.name}
                        </Typography.Paragraph>
                    </Col>
                    <Col>
                        <Typography.Paragraph>
                            <Typography.Text strong>电话：</Typography.Text>
                            {user.tel}
                        </Typography.Paragraph>
                    </Col>
                </Row>))
            }
        </div>
    )
}

export default List
