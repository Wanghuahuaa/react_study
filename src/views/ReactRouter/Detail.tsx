import { LeftOutlined } from "@ant-design/icons";
import { Typography } from "antd";
import { Link, useLocation, useNavigate, useParams, useSearchParams } from "react-router-dom";

const Detail = () => {
    const navigate = useNavigate();
    // searchParams 取参
    const [searchParams, setSearchParams] = useSearchParams()
    // params 取参
    const params = useParams();
    // state 取参
    const location = useLocation();
    const { state } = location
    const { user = {} } = state || {}

    console.log("searchParams: ", searchParams.get("name"), searchParams.get("tel"))
    console.log("params: ", params)
    console.log("state: ", state)

    return (
        <div>
            <Typography.Title level={3}>
                <Link to={`/reactRouter`}>
                    <LeftOutlined className="pointer" style={{ marginRight: 16 }} />
                </Link>
                用户信息
            </Typography.Title>
            <Typography.Paragraph>
                <Typography.Text strong>姓名：</Typography.Text>
                {user.name}
            </Typography.Paragraph>
            <Typography.Paragraph>
                <Typography.Text strong>电话：</Typography.Text>
                {user.tel}
            </Typography.Paragraph>
            <Typography.Paragraph>
                <Typography.Text strong>用户介绍：</Typography.Text>
                {user.introduction}
            </Typography.Paragraph>
        </div>
    )
}

export default Detail
