import { useGlobalContent } from "@/store";
import { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import NoPermission from "../NoPermission";

// 判断页面是否有查看和编辑权限
const PowerWrap = (props: { PageElement: any }) => {
  const { PageElement } = props;
  const { global } = useGlobalContent();
  const menuList = global?.MENU_LIST;
  const location = useLocation();
  const path = location.pathname.substring(5);
  const [pagePower, setPagePower] = useState({ seekpower: false, editpower: false });
  const [hiddenPage, setHiddenPage] = useState(true);

  // 系统配置模块页面不需要判断权限
  const systemSettingRoutes = [
    "/systemSetting/indexDictionary",
    "/zbmb",
    "/upload_input_info",
    "/systemSetting/queryCriteria",
    "/config",
    "/systemSetting/indicatorFieldMaintenance",
    "/system_info",
  ];

  useEffect(() => {
    if (systemSettingRoutes.includes(path)) return setPagePower({ seekpower: true, editpower: true });
    getPagePower();
  }, [path]);

  const getPagePower = async () => {
    // await getPagePermission(path, menuList, setPagePower);
    setTimeout(() => {
      setHiddenPage(false);
    }, 20);
  };

  // console.log('pagePower---', pagePower);

  if (!pagePower?.seekpower && !pagePower?.editpower) return <NoPermission hidden={hiddenPage} />;

  return <PageElement editpower={pagePower?.editpower}></PageElement>;
};

export default PowerWrap;