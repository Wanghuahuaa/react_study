import { randomUUID } from "@/common/utils";
import { routerInfo } from "@/routes";
import { Layout } from "antd";
import { useState } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import ErrorPage from "../ErrorPage";
import HomeMenu from "./HomeMenu";
import "./index.less";

const { Content } = Layout;

const Home = () => {
  const [routerKey, setRouterKey] = useState(randomUUID());

  const changeRouterKey = () => {
    setRouterKey(randomUUID());
  }

  return (<Layout
    className="size100 home-page"
    hasSider
  >
    <HomeMenu changeRouterKey={changeRouterKey} />
    <Content className="home-page-container">
      <div className="all_content">
        <div className={`page-container`} >
          <Routes key={routerKey}>
            <Route
              path="/"
              element={<Navigate replace to={"/hooks/useState"} />}></Route>
            {routerInfo.map((router: any) => {
              const { path, element: Ele, ...rest } = router;
              return (
                <Route
                  key={path}
                  path={path}
                  element={<Ele />}
                ></Route>
              );
            })}
            <Route path="/404" element={<ErrorPage />}></Route>
            <Route path="*" element={<Navigate replace to="/404" />} />
          </Routes>
        </div>
      </div>
    </Content>
  </Layout>);
};

export default Home;
