import { routerInfo } from "@/routes";
import { Layout, Menu, MenuProps } from "antd";
import { useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import "./index.less";
import { menuData } from "./menuData";

const { Sider } = Layout;

// 去掉路由上的 params 参数
const getPathWithoutParams = (str: string) => {
  const i = str.indexOf("/*")
  return {
    pathWithoutParams: i === -1 ? str : str.substring(0, i),
    index: i
  }
}

export default function HomeMenu(props: any) {
  const { changeRouterKey } = props
  const navigate = useNavigate();
  const [openMenuKeys, setOpenMenuKeys] = useState<string[]>([]);
  const [selMenuKey, setSelMenuKey] = useState<string>()
  const location = useLocation();
  const pathname = location.pathname;

  // 当前path 对应的 menuKey  去掉params参数
  const pathMenuKey = useMemo(() => {
    let menuKey
    routerInfo.forEach((item) => {
      const { index, pathWithoutParams } = getPathWithoutParams(item.path)
      if (index === -1) return pathname === item.path ? menuKey = item.path : undefined
      if (pathname.substring(0, index) === item.path.substring(0, index)) menuKey = pathWithoutParams
    })
    return menuKey
  }, [pathname])

  // console.log(37, pathname, pathMenuKey, selMenuKey)

  // 根据路由设置菜单选中
  useEffect(() => {
    if (!pathname) return
    if (pathInMenuList(pathname, menuData) && pathMenuKey) {
      openSubMenu(pathMenuKey);
      setSelMenuKey(pathMenuKey)
      // onSelect(pathname);
    }
  }, [menuData, pathname]);

  //当此时选中的是子菜单时，展开对应的父级菜单
  const openSubMenu = (selMenuKey: string) => {
    let list: string[] = []
    const getPathList = (arr: any[], pPath: string[] = []) => {
      arr.map((item: any) => {
        if (item.key === selMenuKey) {
          list = pPath
        } else if (item?.children?.length) {
          getPathList(item?.children, [...pPath, item.key])
        }
      })
    }
    getPathList(menuData, [])
    setOpenMenuKeys([...new Set([...list, ...openMenuKeys])])
  };

  // 路径是否在才菜单列表中
  const pathInMenuList = (path: string, list: any[]) => {
    let t = false;
    const v = (arr: any[]) => {
      arr.map((item: any) => {
        if (item.children?.length > 0) {
          v(item.children);
        } else if (path === item.key) {
          t = true;
        } else {
          // 处理有 params 参数的
          let i = -1
          routerInfo.forEach((routeItem) => {
            const { index } = getPathWithoutParams(routeItem.path)
            if (index > -1 && item.key === path.substring(0, index)) i = index
          })
          if (i > -1) t = true
        }
      });
    };
    v(list);
    return t;
  };


  // useEffect(() => {
  //   if (!selMenuKey) return navigate("/404")
  //   navigate(selMenuKey);
  // }, [selMenuKey])

  const onSelect = (key: any) => {
    // 点击当前菜单 刷新
    if (key === selMenuKey) return changeRouterKey();
    navigate(key)
    setSelMenuKey(key)
  };

  const onOpenChange: MenuProps['onOpenChange'] = (keys) => {
    return setOpenMenuKeys(keys);
  };

  return (
    <Sider
      width={240}
      // collapsed={MENU_COLLAPSED}
      collapsedWidth={61}
      className="home-sider-menu">
      <Menu
        // className="siderMenu"
        mode="inline"
        // theme="light"
        selectedKeys={selMenuKey ? [selMenuKey] : []}
        items={menuData}
        // onSelect={(e) => onSelect(e.key)}
        // 用 onClick 不用 onSelect？onSelect点击相同菜单不会触发
        onClick={(e: any) => {
          onSelect(e.key)
        }}
        openKeys={openMenuKeys}
        // inlineIndent={20}
        onOpenChange={onOpenChange}
      ></Menu>
    </Sider>
  );
}
