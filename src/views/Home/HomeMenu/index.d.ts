export interface SideMenuProps {
  changeMenuKey: () => void;
  menuData: any[];
}
