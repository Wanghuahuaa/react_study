import { ItemType, MenuItemType } from "antd/es/menu/hooks/useItems";

export const menuData: ItemType<MenuItemType>[] = [
    {
        // key 和路由对应
        key: "/hooks",
        label: "hooks",
        children: [
            {
                key: "/hooks/useState",
                label: "useState",
            },
            {
                key: "/hooks/useEffect",
                label: "useEffect",
            },
            {
                key: "/hooks/useRef",
                label: "useRef",
            },
            {
                key: "/hooks/useCallback",
                label: "useCallback",
            },
            {
                key: "/hooks/useMemo",
                label: "useMemo",
            },
            {
                key: "/hooks/useContext+useReducer",
                label: "useContext+useReducer",
            },
        ]
    },
    {
        key: "/componentsCommunicate",
        label: "组件通信",
        children: [
            {
                key: "/componentsCommunicate/test1",
                label: "父组件向子组件传值",
            },
            {
                key: "/componentsCommunicate/test2",
                label: "子组件向父组件传值",
            },
            {
                key: "/componentsCommunicate/test3",
                label: "兄弟组件传值",
            },
            {
                key: "/componentsCommunicate/test4",
                label: "任意两组件传值",
            },
        ]
    },
    {
        key: "/reactRouter",
        label: "reactRouter",
    },
]