import { useGlobalContent } from "@/store";
import { Col, Dropdown, Layout, Row, Space, Switch } from "antd";
import { useState } from "react";
import defaultPicture from "@/assets/login-image/personImg.png";

const { Header } = Layout

const TopHeader = (props: any) => {
  const { showCockpit, toCockpit, personItems, cockpitPower } = props;
  const { global, globalDispatch } = useGlobalContent();
  const { userInfoData } = global || {};
  const { mc, tx } = userInfoData || {}

  return (
    <Header className="home-top-header">
      <Row className="left-box common_logo" gutter={16} align="middle">
        <Col style={{ height: 40 }}>
          <img src={HY_SWC.logo} className="logo-icon" />
        </Col>
      </Row>

      <Row className="right-box" align="middle" gutter={16}>
        <Col>
          {cockpitPower && <Switch
            checked={showCockpit}
            checkedChildren="驾驶舱"
            unCheckedChildren="驾驶舱"
            onClick={() => toCockpit(true)}
          ></Switch>}
        </Col>
        <Col className="welcome">
          <Space>
            <span>您好，
              <span className="user-name">{mc}</span>
            </span>
          </Space>
        </Col>
        <Col>
          <Dropdown menu={{ items: personItems }} arrow>
            {tx ?
              <img src={HY_SWC.fileBaseURL + "/" + tx} className="user-avatar pointer" />
              :
              <img src={defaultPicture} className="user-avatar pointer" />
            }
          </Dropdown>
        </Col>
      </Row>
    </Header >
  );
};

export default TopHeader;
