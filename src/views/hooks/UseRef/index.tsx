import { Button } from "antd"
import { useRef } from "react"

function UseRef() {
    const boxRef = useRef<HTMLDivElement>(null)

    const getBox = () => {
        console.log(boxRef)
        boxRef.current?.children[0].setAttribute("style", "color: pink")
    }

    return (
        <div ref={boxRef} style={{ width: 200, height: 200 }} className="common-border v-flex-center">
            <h2 >Box</h2>
            <Button onClick={getBox}>获取box</Button>
        </div>
    )
}

export default UseRef
