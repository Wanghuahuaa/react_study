import { Button } from "antd"
import { useState } from "react"

function UseCallback() {
    const [count, setCount] = useState<number>(0)

    const add = () => {
        setCount(count + 1)
    }

    const getData = () => { }
    // const getData = useCallback(() => { }, [])

    return (
        <div style={{ width: 300, height: 300 }} className="common-border v-flex-center">
            <h2>count: {count}</h2>
            <Button onClick={add}>count+1</Button>
            <Child
            // getData={getData}
            ></Child>
        </div>
    )
}

export default UseCallback

const Child =
    // memo(
    (props: any) => {
        console.log("子组件更新")
        return <div style={{ width: 150, height: 150, marginTop: 24, background: "pink" }} className="v-flex-center">
            子组件
        </div>
    }
// )
