import { Button } from "antd"
import { useRef, useState } from "react"

function UseState() {
    const [count1, setCount1] = useState<number>(0)
    // 每次render都会重新初始化
    let count2 = 0
    // 只会初始化一次
    const count3 = useRef<number>(0)

    const add = () => {
        setCount1(count1 + 1)
        // setCount1((prev: number) => prev + 1)

        // console.log(15, count1)
        // setTimeout(() => {
        //     console.log(17, count1)
        // }, 2000)
    }

    const getCount = () => {
        console.log("count1:", count1, " count2:", count2, " count3:", count3.current)
    }

    return (
        <div className="use-state-test">
            <h2>
                count1:
                {count1}
            </h2>
            <Button onClick={add}>count1+1</Button>
            <h2>
                count2:
                {count2}
            </h2>
            <Button onClick={() => count2++}>count2+1</Button>
            <h2>
                count3:
                {count3.current}
            </h2>
            <Button onClick={() => count3.current++}>count3+1</Button>
            <br />
            <br />
            <br />
            <Button onClick={getCount}>获取所有count值</Button>
        </div>
    )
}

export default UseState
