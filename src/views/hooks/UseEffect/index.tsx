import { Button } from "antd"
import { useEffect, useState } from "react"
let interval: NodeJS.Timeout | null = null

const UseEffect = () => {
    const [time, setTime] = useState<Date>(new Date())
    const [count, setCount] = useState<number>(0)

    useEffect(() => {
        // console.log("useEffect setup执行")
        setTime(new Date())
        interval = setInterval(() => {
            setTime(new Date())
            console.log("14--count:", count)
        }, 1000)
        return () => {
            // console.log("useEffect cleanup清理函数方法执行")
            if (interval) clearInterval(interval)
        }
    }, [])
    // }, [count])

    // 每次渲染执行
    // useEffect(() => {
    //     console.log(25)
    //     return () => {
    //         console.log(27)
    //     }
    // })

    // 首次渲染完成执行
    // useEffect(() => {
    //     console.log(33)
    //     return () => {
    //         console.log(35)
    //     }
    // }, [])

    // 首次渲染完成 及 count 改变时执行
    // useEffect(() => {
    //     console.log(41)
    //     return () => {
    //         console.log(43)
    //     }
    // }, [count])

    const stop = () => {
        if (interval) clearInterval(interval)
    }

    return (
        <div className="use-state-test">
            <h3>
                当前时间：{time.toLocaleString()}
            </h3>
            <Button onClick={stop}>暂停</Button>
            <h3>
                count: {count}
            </h3>
            <Button onClick={() => setCount(count + 1)}>count+1</Button>
        </div>
    )
}

export default UseEffect
