import { Button } from "antd"
import { useMemo, useState } from "react"

function UseMemo() {
    const [a, setA] = useState<number>(0)
    const [b, setB] = useState<number>(0)
    const [c, setC] = useState<number>(0)

    const sum = useMemo(() => {
        console.log('useMemo')
        return a + b
    }, [a, b])

    const sum1 = () => {
        console.log('sum1')
        return a + b
    }

    return (
        <div style={{ width: 350, height: 350 }} className="common-border v-flex-center">
            <h3>a: {a}</h3>
            <Button onClick={() => setA(a + 1)}>a+1</Button>
            <h3>b: {b}</h3>
            <Button onClick={() => setB(b + 1)}>b+1</Button>
            <h3>c: {c}</h3>
            <Button onClick={() => setC(c + 1)}>c+1</Button>
            <h3>sum: {sum}</h3>
            <h3>sum1: {sum1()}</h3>
        </div>
    )
}

export default UseMemo