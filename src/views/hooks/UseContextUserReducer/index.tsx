import ComponentA from "./ComponentA"
import { GlobalContainer } from "./store"

function UseContextUserReducer() {
    return (
        <GlobalContainer>
            <ComponentA></ComponentA>
        </GlobalContainer>
    )
}

export default UseContextUserReducer
