import { Row } from "antd";
import { useContext } from "react";
import { GlobalContext } from "./store";

const ComponentB = () => {
    const { global } = useContext(GlobalContext);
    const { inputValue } = global || {}

    return (
        <Row className="v-flex-center" justify="center" align="middle" style={{ width: 250, height: 250, background: "#00f1ff4d" }}>
            <h3>ComponentB</h3>
            <div>D组件输入内容: <b>{inputValue || "--"}</b></div>
        </Row>
    )
}

export default ComponentB
