import { createContext, useReducer } from "react";

const initData = {
    inputValue: ""
};

interface GlobalContainerProps {
    children: any;
}

// 1. 创建一个context
export const GlobalContext = createContext({} as any);

// 全局容器
export const GlobalContainer = (props: GlobalContainerProps) => {

    const reducer = (state: any, action: any) => {
        console.log(18, action)
        let newState = { ...state };
        const { type, data } = action;
        if (!type) {
            new Error("not type");
        }
        newState[type] = data;
        return newState;
    };

    const [global, dispatch] = useReducer(reducer, initData);

    // 2. 将组件使用 Provider 包裹起来  并通过 value 向下传值
    return (
        <GlobalContext.Provider
            value={{
                global,
                dispatch,
            }}
        >
            {props.children}
        </GlobalContext.Provider>
    );
};

export default GlobalContainer;

export type globalInitType = typeof initData;