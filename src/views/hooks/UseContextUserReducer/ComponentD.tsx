import { Input } from "antd";
import { useContext } from "react";
import { GlobalContext } from "./store";

const ComponentD = () => {
    // 3. 使用 useContext 取值
    const { global, dispatch } = useContext(GlobalContext);
    const { inputValue } = global || {}

    const updateValue = (newValue: string) => {
        dispatch({
            data: newValue,
            type: "inputValue",
        });
    }

    return (
        <div className="v-flex-center h-padding-8" style={{ width: 180, height: 160, background: "#ffffff" }}>
            <h5>ComponentD</h5>
            <div>
                <Input value={inputValue} onChange={(e) => updateValue(e.target.value)}></Input>
            </div>
        </div>
    )
}

export default ComponentD
