import ReactDOM from 'react-dom/client'
import { HashRouter } from 'react-router-dom'
import App from './App.tsx'
import './index.less'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <HashRouter>
    <App></App>
  </HashRouter>,
)
