import ReactRouter from "@/views/ReactRouter";
import Test1 from "@/views/componentsCommunicate/Test1";
import Test2 from "@/views/componentsCommunicate/Test2";
import Test3 from "@/views/componentsCommunicate/Test3";
import Test4 from "@/views/componentsCommunicate/Test4";
import UseCallback from "@/views/hooks/UseCallback";
import UseContextUserReducer from "@/views/hooks/UseContextUserReducer";
import UseEffect from "@/views/hooks/UseEffect";
import UseMemo from "@/views/hooks/UseMemo";
import UseRef from "@/views/hooks/UseRef";
import UseState from "@/views/hooks/UseState";

interface RouterInfo {
  path: string; // 路径
  label: string;
  element?: any; //组件
  children?: RouterInfo[]
}

export const routerInfo: RouterInfo[] = [
  {
    path: "/hooks/useState",
    label: "useState",
    element: UseState,
  },
  {
    path: "/hooks/useEffect",
    label: "useEffect",
    element: UseEffect,
  },
  {
    path: "/hooks/useRef",
    label: "useRef",
    element: UseRef,
  },
  {
    path: "/hooks/useCallback",
    label: "useCallback",
    element: UseCallback,
  },
  {
    path: "/hooks/useMemo",
    label: "useMemo",
    element: UseMemo,
  },
  {
    path: "/hooks/useContext+useReducer",
    label: "useContext+useReducer",
    element: UseContextUserReducer,
  },
  {
    path: "/reactRouter/*",
    label: "reactRouter",
    element: ReactRouter,
  },
  {
    path: "/componentsCommunicate/test1",
    label: "父组件向子组件传值",
    element: Test1,
  },
  {
    path: "/componentsCommunicate/test2",
    label: "子组件向父组件传值",
    element: Test2,
  },
  {
    path: "/componentsCommunicate/test3",
    label: "兄弟组件传值",
    element: Test3,
  },
  {
    path: "/componentsCommunicate/test4",
    label: "任意两组件传值",
    element: Test4,
  },
]
