// 根据属性值找到某一行的数据
export const getRowByField = (
  list: any[],
  value: any,
  filed: string = "tid"
) => {
  let record
  const v = (arr: any[]) => {
    arr.forEach((item: any) => {
      if (item[filed] === value) return (record = { ...item })
      if (item.children?.length) v(item.children)
    })
  }
  v(list)
  return record
}

/** 生成 UUID */
export function randomUUID() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8
    return v.toString(16)
  })
}
