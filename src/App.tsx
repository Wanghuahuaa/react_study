import "./App.less"
import Home from "./views/Home"

function App() {

  return (
    <div className="app">
      <Home />
    </div>
  )
}

export default App
