[演示项目地址](git@gitlab.com:Wanghuahuaa/react_study.git)

## 项目搭建

1. `yarn create vite test-project `  根据提示选择配置（react + typescript）
2. `yarn `  安装依赖（后续安装 `yarn add xxx [-D]`）
3. `yarn dev` 启动项目

## JSX

JSX是 JavaScript 语法扩展，可以让你在 JavaScript 文件中书写类似 HTML 的标签

#### jsx使用注意事项

- 组件内只能返回一个根元素

- 在大括号内编写 JavaScript

  ````javascript
  const userName = "王花花"
  const element = <span>{userName}</span>
  ````

## 类组件和函数式组件

#### 类组件

```javascript
class Hello extends React.Component{
    render(){
        return <h1>hello</h1>
    }
}
```

#### 函数式组件

```javascript
function Hello(){
    return <h1>hello</h1>
}
// 组件调用方式：
<Hello></Hello>
```

## 常用hooks

#### useState

用于保存和更新组件的状态

```javascript
const [state, setState] = useState(0)

setState(1)
setState((prev) => prev + 1)
```

特性：

1. `set`函数调用更新state，触发组件重新渲染
2. 异步更新

#### useEffect

useEffect是React中的一个钩子函数，用于处理副作用操作。副作用是指在组件渲染过程中，可能会对外部环境产生影响的操作，比如数据获取、订阅事件、操作DOM等。

`useEffect(setup, dependencies?)`

setup: 处理 Effect 的函数， 可return一个cleanup清理函数
dependencies: setup 函数中引用的所有响应式值的列表

```javascript
useEffect(() => {
    console.log("useEffect 方法执行")
    return () => {
        console.log("useEffect cleanup清理函数方法执行")
   }
}, [count])
```

setup和cleanup执行时机：当组件被添加到 DOM 的时候，React 将运行 setup 函数。在每次依赖项变更组件重新渲染后，React 将首先使用旧值运行 cleanup 函数，然后使用新值运行 setup 函数。在组件从 DOM 中移除后，React 将最后一次运行 cleanup 函数。

#### useRef

用于引用 DOM 元素或者保存值

```javascript
const boxRef = useRef(null)
<div ref={boxRef}></div>

// 取值
countRef.current 
```

#### useCallback

用于缓存函数，避免组件渲染时重复创建函数

```javascript
const getData = useCallback(() => {
    // 函数处理逻辑
}, []) // 数组中放依赖项 依赖项变化时重新创建函数
```

#### useMemo

用于缓存计算结果，避免组件渲染时重复计算

```javascript
const newData = useMemo(() => {
    // 返回计算结果
    return a+b
}, [a, b]) // 数组中放依赖项
```

#### useContext

用于context所包含的组件间共享数据

```javascript
// 1. 使用`createContext`创建一个context
const GlobalContext = createContext({} as any);
// 2. 将组件使用 `Context.Provider` 包裹起来 并用 value 传值
<GlobalContext.Provider
	value={{
           global,
           dispatch,
       }}
>
	<MyComponent></MyComponent>
</GlobalContext.Provider>
 // 3. 在组件中使用 useContext 取值
const { global, dispatch } = useContext(GlobalContext);
```

#### useReducer

用于更复杂的状态管理，常和useContext结合使用，实现全局数据管理

```javascript
// state: 当前状态值 
// dispatch： 用于更新 state 并触发组件的重新渲染 需要传入一个 action 作为参数 触发reducer中的处理逻辑
// reducer: 一个用于更新 state 的函数  参数为 state 和 action，返回更新后的 state
// initData: state 的初始值
const [state, dispatch] = useReducer(reducer, initData);
const reducer = (state: any, action: any) => {
        let newState = { ...state };
        const { type, data } = action;
        if (!type) {
            new Error("not type");
        }
        newState[type] = data;
        return newState;
    };

dispatch({
	type: "inputValue",
    data: newValue,
});
```

#### useImperativeHandle

*useImperativeHandle* + *forwardRef* + *useRef* 配合使用 实现父组件获取子组件的方法或变量（见组件通信部分）

## 组件通信

#### 父组件向子组件传值

使用props传值

```javascript
// 父组件传值
<ComponentB pageInfo={pageInfo}></ComponentB>

// 子组件中取值
const { pageInfo } = props
```

#### 子组件向父组件传值

- 使用回调函数

  ```javascript
  // 父组件
  const onValue1Change = (value: string) => {
        setValue1(value)
  }
  // 调用子组件时 将 onValue1Change 作为回调函数传递给子组件
  <ComponentB value1={value1} onValue1Change={onValue1Change}></ComponentB>
  
  // 子组件
  const { value1, onValue1Change } = props
  <Input value={value1} onChange={(e) => onValue1Change(e.target.value)}></Input>
  ```

- 使用 `useRef`+`forwardRef`+`useImperativeHandle`

  ```javascript
  // 子组件
  // 使用 forwardRef 包裹
  const ComponentB = forwardRef((props: IComponentB, ref) => {
      const [value2, setValue2] = useState<string>("")
      // 使用 useImperativeHandle 将变量或方法暴露给父组件
      useImperativeHandle(ref, () => ({
          value2
      }));
      return <Input value={value2} onChange={(e) => setValue2(e.target.value)}></Input>
  })
  export default ComponentB
  
  // 父组件
  // 调用子组件时 传递 ref 
  const componentBRef = useRef<any>()
  // 取值
  const value2 = componentBRef.current.value2
  <ComponentB ref={componentBRef}>
  ```

#### 兄弟组件传值

"状态提升"

```javascript
// 将状态保存在公共父组件中
const ComponentA = () => {
    const [count, setCount] = useState<number>(0)
    return (
        <Row>
        	//传给子组件
           <ComponentB count={count}></ComponentB>
           <ComponentC setCount={setCount}></ComponentC>
        </Row >
    )
}
```

#### 任意两组件传值

- 使用Context（见hooks部分）

- 发布订阅`pubsub-js`

  ```javascript
  // 订阅
  PubSub.subscribe("GET_INPUT_VALUE", (_, data: any) => {
      setInputValue(data.value);
  });
  // 发布
  PubSub.publish("GET_INPUT_VALUE", { value });
  ```

## react路由(HashRouter)

#### 实现功能

通过配置路径和组件的对应关系，实现页面之间的跳转

#### 基本用法

```javascript
<HashRouter>
    <Routes>
    	<Route path="/" element={<List />}></Route>
		<Route path="/detail/:id" element={<Detail />}></Route>
		<Route path="/404" element={<ErrorPage />}></Route>
		<Route path="*" element={<Navigate replace to="/404" />} />
   </Routes>
</HashRouter>

// 路由跳转
navigate(`/reactRouter/detail`)
navigate(1)
navigate(-1)
<Link to={`/reactRouter`}>返回</Link>
```

#### 路由传参

- searchParams

  ```javascript
  // 用法
  navigate(`/reactRouter/detail?name=${user.id}&tel=${user.tel}`)
  
  // 取参
  const [searchParams, setSearchParams] = useSearchParams()
  const name = searchParams.get("name")
  ```

- params

  ```javascript
  // 用法
  navigate(`/reactRouter/detail/${user.id}`)
  // 配合使用
  <Route path="/detail/:id" element={<Detail />}></Route>
  
  // 取参
  const params = useParams()
  const id = params.id
  ```

- state

  ```javascript
  // 用法
  navigate(`/reactRouter/detail`, { state: { user } })
  
  // 取参
  const location = useLocation();
  const { state } = location
  const { user = {} } = state || {}
  ```



